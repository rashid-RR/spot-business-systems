export let APP_ICONS = [
    `contact-info`, `sales`, `sales-copy`, `printer`,`add-more-button-icon`,
    `edit`, `delete`, `chat`, `show`, `graph-page`, `promotion-page`,
    `order-history`, `garment`, `suitcase`, `graph`, `conversation`,
    `lookup`, `settings`, `notification-icon`, `crm`, `promotions`,
    `credit-card`, `preferences`, `referral`
]