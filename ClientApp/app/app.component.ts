import { Component } from '@angular/core';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
import {APP_ICONS} from "./app-icons"
@Component({
  selector: 'spot-app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SPOT Customer Console';
  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    APP_ICONS.forEach(icon => {
      this.matIconRegistry.addSvgIcon(
        `spot-${icon}`,
        this.domSanitizer.bypassSecurityTrustResourceUrl(`../assets/images/icons/${icon}.svg`)
      );
    });
  }
}
