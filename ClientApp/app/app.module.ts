import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { SharedModule } from './shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpService } from '../services/http/http.service';
import { CacheInterceptor } from '../services/http/interceptors/cache.interceptor';
import { HttpCacheService } from '../services/http/http-cache.service';
import { LogResponseInterceptor } from '../services/http/interceptors/log-response.interceptor';
import { AuthenticateInterceptor } from '../services/http/interceptors/authenticate.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    AppLayoutComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule,
    AppRoutingModule, SharedModule
  ],
  providers: [
    HttpCacheService,
    // { provide: ErrorHandler, useClass: SpotErrorHandlerService },
    { provide: HTTP_INTERCEPTORS, useClass: LogResponseInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthenticateInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true },
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
