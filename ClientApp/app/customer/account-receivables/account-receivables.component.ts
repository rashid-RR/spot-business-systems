import { Component, OnInit } from '@angular/core';

export interface AccountType {
  value: string;
  viewValue: string;
}
export interface counter {
  value: string;
  viewValue: string;
}
export interface route {
  value: string;
  viewValue: string;
}
export interface payment {
  value: string;
  viewValue: string;
}
export interface method {
  value: string;
  viewValue: string;
}
export interface statement {
  value: string;
  viewValue: string;
}
export interface account_type2 {
  value: string;
  viewValue: string;
}
export interface account_status {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'spot-app-customer-account-receivables',
  templateUrl: './account-receivables.component.html',
  styleUrls: ['./account-receivables.component.scss']
})
export class CustomerAccountReceivablesComponent implements OnInit {

  constructor(){  }

  public enabledValue:boolean = true;
  public onClick:boolean = false;
  public text_color:boolean = false;
  public onClick3:boolean = false;
  public text_color3:boolean = false;
  public credit_card_box2:boolean = true;
  public credit_card_box1:boolean = false;
  enabled = false;


  type: AccountType[] = [
    {value: 'none1-0', viewValue: 'Master'},
    {value: 'yes-1', viewValue: 'Yess'},
    {value: 'Demo-1', viewValue: 'Demo'}
  ];
  countr: counter[] = [
    {value: 'none1-0', viewValue: '(Store Default)'},
    {value: 'yes-1', viewValue: 'Yess'},
    {value: 'Demo-1', viewValue: 'Demo'}
  ];
  rot: route[] = [
    {value: 'none1-0', viewValue: '(Store Default)'},
    {value: 'yes-1', viewValue: 'Yess'},
    {value: 'Demo-1', viewValue: 'Demo'}
  ];
  pay: payment[] = [
    {value: 'none1-0', viewValue: 'Default to Account,use CC to pay'},
    {value: 'yes-1', viewValue: 'Yess'},
    {value: 'Demo-1', viewValue: 'Demo'}
  ];
  meth: method[] = [
    {value: 'none1-0', viewValue: 'Email(.PDF file)'},
    {value: 'yes-1', viewValue: 'Yess'},
    {value: 'Demo-1', viewValue: 'Demo'}
  ];
  stmt: statement[] = [
    {value: 'none1-0', viewValue: 'Email'},
    {value: 'yes-1', viewValue: 'Yess'},
    {value: 'Demo-1', viewValue: 'Demo'}
  ];
  ac_type: account_type2[] = [
    {value: 'none1-0', viewValue: 'Checking'},
    {value: 'yes-1', viewValue: 'Yess'},
    {value: 'Demo-1', viewValue: 'Demo'}
  ];
  ac_status: account_status[] = [
    {value: 'none1-0', viewValue: 'Enabled'},
    {value: 'yes-1', viewValue: 'Yess'},
    {value: 'Demo-1', viewValue: 'Demo'}
  ];


  clickEvent(){
    this.onClick = true;
    this.text_color = false;
  }
  clickEvent2(){
    this.onClick = true;
    this.text_color = true;
  }

  clickEvent3(){
    this.onClick3 = true;
    this.text_color3 = false;
  }
  clickEvent4(){
    this.onClick3 = true;
    this.text_color3 = true;
  }
  enableToggle(){
    this.enabledValue = !this.enabledValue;
  }

ngOnInit(){

}

}
