import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../app/shared';
import { FormGroup } from '@angular/forms';
import { RootObjectResponse } from '../../../models/root-object.interface';
import { Customer } from '../../../models/customer/customer.interface';
import { SpotHttpError } from '../../../models/spot-http-error.model';
import { ActivatedRoute } from '@angular/router';
import { Constants } from '../../../app/shared/constants';
import { throwError } from 'rxjs';
import { EditCreditcardModalComponent } from '../edit-creditcard-modal/edit-creditcard-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'spot-app-customer-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss']
})
export class CustomerCreditCardComponent implements OnInit {
  public group: FormGroup;
  constructor(private customerService: CustomerService,
    private route: ActivatedRoute,
    public dialog: MatDialog) {
    this.group = this.customerService.creditCardForm;
  }

  ngOnInit() {
    const resolvedData: RootObjectResponse<Customer> | SpotHttpError = this.route.snapshot.data[Constants.RESOLVED_CUSTOMER_KEY];
    if (resolvedData instanceof SpotHttpError) {
      console.log(`Credit card component error ${resolvedData.message}`);
    } else {
      if (!resolvedData.Failed) {
      }
    }
  }

  enableToggle() {
    const en = this.customerService.creditCardForm.get('enabled');
    en !== null ? en.setValue(!en.value) : throwError('Error, could not find the enabled form element within the credit card component');
  }

  get enabledValue(): boolean {
    const enabled = this.customerService.creditCardForm.get('enabled');
    return enabled !== null ? enabled.value : throwError('');
  }

  editCCard(): void {
    this.dialog.open(EditCreditcardModalComponent, {panelClass: 'custom-modalbox', maxWidth: 550, data: {  } });
  }
}
