import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CustomerRoutes } from './customer.routing';
import { MatNativeDateModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { CustomerInformationComponent } from './information/information.component';
import { SharedModule } from '../shared/shared.module';
import { CustomerAddressModalComponent } from './address-modal/address-modal.component';
import { CustomerPreferenceComponent } from './preference/preference.component';
import { CustomerCreditCardComponent } from './credit-card/credit-card.component';
import { CustomerReferralComponent } from './referral/referral.component';
import { CustomerPromotionComponent } from './promotion/promotion.component';
import { CustomerAccountReceivablesComponent } from './account-receivables/account-receivables.component';
import { CustomerNotificationsComponent } from './notifications/notifications.component';
import { CustomerSettingComponent } from './setting/setting.component';
import { CustomerLookupComponent } from './lookup/lookup.component';
import { CustomerPricingComponent } from './pricing/pricing.component';
import { CustomerCrmComponent } from './crm/crm.component';
import { CustomerLayoutComponent } from './layout/layout.component';
import { MessageModalComponent } from './message-modal/message-modal.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { CustomerService } from '../shared';
import { CustomerResolverService } from '../../services/resolvers/customer-resolver.service';
import { EditMemoModalComponent } from './edit-memo-modal/edit-memo-modal.component';
import { EditCreditcardModalComponent } from './edit-creditcard-modal/edit-creditcard-modal.component';

@NgModule({
  imports: [
    CommonModule, SharedModule, MatExpansionModule, MatDatepickerModule,
    MatNativeDateModule,
    RouterModule.forChild(CustomerRoutes),
    CKEditorModule
  ],
  entryComponents: [CustomerAddressModalComponent, MessageModalComponent, EditMemoModalComponent, EditCreditcardModalComponent],
  declarations: [CustomerInformationComponent, CustomerAddressModalComponent,
    CustomerPreferenceComponent, CustomerCreditCardComponent, CustomerReferralComponent,
    CustomerPromotionComponent, CustomerAccountReceivablesComponent, CustomerNotificationsComponent,
    CustomerSettingComponent, CustomerLookupComponent, CustomerPricingComponent, CustomerCrmComponent,
    CustomerLayoutComponent, MessageModalComponent, EditMemoModalComponent, EditCreditcardModalComponent],
  providers: [CustomerService, CustomerResolverService]
})
export class CustomerModule { }
