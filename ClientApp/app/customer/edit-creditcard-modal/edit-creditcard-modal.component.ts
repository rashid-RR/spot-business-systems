import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Inject } from '@angular/core';

@Component({
  selector: 'spot-app-edit-creditcard-modal',
  templateUrl: './edit-creditcard-modal.component.html',
  styleUrls: ['./edit-creditcard-modal.component.scss']
})
export class EditCreditcardModalComponent implements OnInit {

  type = 'primary';
  types = [
    { value: 'primary', viewValue: 'Primary' },
  ];
  name = 'Can Yelok';
  expiery = '';
  cvc = '';
  cardNumer = '';
  cardValidation = [
    ['Visa', /^4/],
    ['Mastercard', /^5[1-5]/],
    ['American Express', /^3(4|7)/],
    ['Discover', /^6011/]
  ];
  cardValidated = '';

  constructor(
    public dialogRef: MatDialogRef<EditCreditcardModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  cancel() {
    this.dialogRef.close();
  }

  cardChange() {
    // this.cardNumer
    // this.cardValidated
    this.cardValidated = '';
    this.cardValidation.forEach((e: any) => {
      console.log('(e[1]).test(this.cardNumer)', (e[1]).test(this.cardNumer));
      if ( (e[1]).test(this.cardNumer) ) {
        this.cardValidated = e[0];
      }
    });
  }
}
