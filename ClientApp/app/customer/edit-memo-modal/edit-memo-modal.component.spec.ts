import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMemoModalComponent } from './edit-memo-modal.component';

describe('EditMemoModalComponent', () => {
  let component: EditMemoModalComponent;
  let fixture: ComponentFixture<EditMemoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMemoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMemoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
