declare var require: any;
import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Inject } from '@angular/core';

@Component({
  selector: 'spot-app-edit-memo-modal',
  templateUrl: './edit-memo-modal.component.html',
  styleUrls: ['./edit-memo-modal.component.scss']
})
export class EditMemoModalComponent implements OnInit {

  subject = '-';
  // ckeConfig: any;
  public Editor = require('@ckeditor/ckeditor5-build-decoupled-document');

  logDisplay = true;
  DisplaySubject = true;
  memo = '';
  memos = [
    { value: '', viewValue: 'None' }
  ];

  status = 'enabled';
  statuses = [
    { value: 'enabled', viewValue: 'Enabled' },
  ];

  animation = 'dropoff';
  animations = [
    { value: 'dropoff', viewValue: 'DropOff' },
  ];

  frequency = 'always';
  frequencies = [
    { value: 'always', viewValue: 'Always' },
  ];

  printOnInvoice = false;
  printOnStatement = true;
  printOnBagTag = true;

  constructor(
    public dialogRef: MatDialogRef<EditMemoModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  cancel() {
    this.dialogRef.close();
  }

  public onEditorReady(editor: { ui: { view: { editable: { element: {
    parentElement: { insertBefore: (arg0: any, arg1: any) => void; }; }; };
    toolbar: { element: any; }; }; }; } ) {
    editor.ui.view.editable.element.parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.view.editable.element
    );
  }
}
