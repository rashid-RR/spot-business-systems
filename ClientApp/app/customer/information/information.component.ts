import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../shared';
import { MatDialog } from '@angular/material';
import { CustomerAddressModalComponent } from '../address-modal/address-modal.component';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RootObjectResponse } from '../../../models/root-object.interface';
import { Customer } from '../../../models/customer/customer.interface';
import { SpotHttpError } from '../../../models/spot-http-error.model';
import { Phone } from '../../../models/customer/phone.interface';
import { AddressAndLocation } from '../../../models/customer/address-and-location.interface';
import { Constants } from '../../../app/shared/constants';
import { CustomerDataService } from 'ClientApp/services/customer/customer-data.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'spot-app-customer-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class CustomerInformationComponent implements OnInit {
  private componentName = 'customer information component.';
  private customer!: Customer;
  passwordVisible: Boolean = false;

  constructor(public customerService: CustomerService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private customerDataService: CustomerDataService) { }

  ngOnInit() {
    // Ensuring the HTTP calls succeeds first through a resolver.
    const resolvedData: RootObjectResponse<Customer> | SpotHttpError = this.route.snapshot.data[Constants.RESOLVED_CUSTOMER_KEY];
    if (resolvedData instanceof SpotHttpError) {
      console.log(`Customer info component error: ${resolvedData}`);
    } else {
      if (resolvedData && !resolvedData.Failed) {
        this.customer = resolvedData.ReturnObject;
      } else {
        console.log(`Server error. Message: ${resolvedData}`);
      }
    }
    // If the Customer object contains values, assign them to local variables.
    if (this.customer) {
      const _firstName = this.customer.FirstName;
      const _lastName = this.customer.LastName;
      const _customerType = this.customer.CustomerType;
      const _vip = this.customer.VIP;
      let _emails: string[];
      let _phones: Phone[] | null;
      let _addresses: AddressAndLocation[];
      if (this.customer.EmailAddresses && this.customer.EmailAddresses.length > 0) {
        _emails = this.customer.EmailAddresses;
      } else {
        _emails = [''];
      }
      if (this.customer.Phones && this.customer.Phones.length > 0) {
        _phones = this.customer.Phones;
      } else {
        _phones = null;
      }
      if (this.customer.AddressesAndLocations && this.customer.AddressesAndLocations.length > 0) {
        _addresses = this.customer.AddressesAndLocations;
        while (this.addressData && this.addressData.length > 0 && this.addressData.length !== 0) {
          this.addressData.removeAt(0);
        }
        for (const addr of _addresses) {
          if (addr.Address) {
            this.addressData.push(this.fb.group({
              addressee: addr.Address.Addressee,
              line1: addr.Address.Address1,
              line2: addr.Address.Address2,
              city: addr.Address.City,
              state: addr.Address.State,
              zip: addr.Address.Zip
            }));
          }
        }
      }
      // Patch Initial Values
      this.customerService.informationForm.patchValue({
        firstName: _firstName,
        lastName: _lastName,
        customerType: _customerType,
        vip: _vip
      });

      // Patch Emails
      if (_emails.length > 0) {
        while (this.emailData && this.emailData.length > 0 && this.emailData.length !== 0) {
          this.emailData.removeAt(0);
        }
        for (const e of _emails) {
          this.emailData.push(this.fb.group({
            email: e
          }));
        }
      }

      // Patch phones
      const phoneLength = _phones !== null ? _phones.length : 0;
      if (phoneLength > 0) {
        while (this.phoneData && this.phoneData.length > 0 && this.phoneData.length !== 0) {
          this.phoneData.removeAt(0);
        }
        if (_phones !== null) {
          for (const p of _phones) {
            this.phoneData.push(this.fb.group({
              phoneType: p.PhoneType,
              phoneMask: p.PhoneMask,
              number: p.Number,
              extension: p.Extension
            }));
          }
        }
      }
    }
  }

  get addressData(): FormArray {
    return <FormArray>this.customerService.informationForm.get('addresses');
  }

  get phoneData(): FormArray {
    return <FormArray>this.customerService.informationForm.get('phones');
  }

  get emailData(): FormArray {
    return <FormArray>this.customerService.informationForm.get('emails');
  }

  get isVip(): boolean {
    const vip = this.customerService.informationForm.get('vip');
    return vip !== null ? vip.value : throwError(`Could not find the VIP form control on the ${this.componentName}`);
  }

  addPhone(): void {
    this.phoneData.push(this.buildPhone());
  }

  addEmail(): void {
    this.emailData.push(this.buildEmail());
  }

  buildEmail(): FormGroup {
    return this.fb.group({
      email: ''
    });
  }

  buildAddress(): FormGroup {
    return this.fb.group({
      addressee: '',
      line1: '',
      line2: '',
      city: '',
      state: '',
      zip: ''
    });
  }

  buildPhone(): FormGroup {
    return this.fb.group({
      phoneType: '',
      number: '',
      extension: '',
      phoneMask: ''
    });
  }

  addAddress() {
    const dialogRef = this.dialog.open(CustomerAddressModalComponent, { panelClass: 'custom-modalbox',maxWidth: 400, data: { title: 'Add New Address' } });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const addresses = this.customerService.informationForm.controls.addresses.value;
        addresses.push(result);
        this.customerService.informationForm.controls.addresses.setValue(addresses);
      }
    });
  }

  editAddress(i: any, address: any) {
    const dialogRef = this.dialog.open(CustomerAddressModalComponent, { panelClass: 'custom-modalbox',maxWidth: 400, data: { title: 'Update Address', address: address } });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const addresses = this.customerService.informationForm.controls.addresses.value;
        addresses.splice(i, 1, result);
        this.customerService.informationForm.controls.addresses.setValue(addresses);
      }
    });
  }

  deleteAddress(i: any) {
    const addresses = this.customerService.informationForm.controls.addresses.value;
    addresses.splice(i, 1);
    this.customerService.informationForm.controls.addresses.setValue(addresses);
  }

  saveCustomerInformation(): void {
    if (this.customerService.informationForm.valid) {
      try {
        if (this.customer) {
          const firstName = this.customerService.informationForm.get('firstName');
          const lastName = this.customerService.informationForm.get('lastName');
          const customerType = this.customerService.informationForm.get('customerType');
          const vip = this.customerService.informationForm.get('vip');
          const phones = this.customerService.informationForm.get('phones');
          this.customer.FirstName = firstName !== null ? firstName.value : throwError(`Error, could not find control firstName in ${this.componentName}`);
          this.customer.LastName = lastName !== null ? lastName.value : throwError(`Error, could not find control lastName in ${this.componentName}`);
          this.customer.CustomerType = customerType !== null ? customerType.value :
            throwError(`Error, could not find control customerType in ${this.componentName}`);
          this.customer.VIP = vip !== null ? vip.value : throwError(`Error, could not find control vip in ${this.componentName}`);
          this.customer.Phones = phones !== null ? phones.value : throwError(`Error, could not find control phones in ${this.componentName}`);
          this.customerDataService.saveCustomer(this.customer);
        }
      } catch (e) {
        console.log(`Hit the catch block during API save call with exception: ${e}`);
      } finally {
        console.log('Finally hit on API call');
      }
    }
  }
}
