import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CustomerService } from '../../shared';
import { MessageModalComponent } from '../message-modal/message-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'spot-app-customer-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class CustomerLayoutComponent implements OnInit {

  drawerOpened = true;

  constructor(private customerService: CustomerService, public dialog: MatDialog) {

  }
  @ViewChild('drawer')

  drawer!: ElementRef;

  toggleDrawer() {
    this.drawerOpened = !this.drawerOpened;
    const SideItems: [] = this.drawer.nativeElement.querySelectorAll('.mat-list-item-content');
    SideItems.forEach((SideItem: any) => {
      SideItem.querySelector('.mat-list-text').style.display = this.drawerOpened ? 'inherit' : 'none';
    });
  }

  ngOnInit() { }

  get customerName() {
    return (this.customerService.informationForm.controls.firstName.value || '') + ' ' + (this.customerService.informationForm.controls.lastName.value || '');
  }
  openMessageModal() {
    this.dialog.open(MessageModalComponent, {panelClass: 'custom-modalbox', maxWidth: 570, data: { title: 'Add New Address' } });
  }
}
