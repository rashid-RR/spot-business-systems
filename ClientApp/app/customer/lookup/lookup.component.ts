import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormBuilder } from '@angular/forms';
import { CustomerService } from 'ClientApp/app/shared';

@Component({
  selector: 'spot-app-customer-lookup',
  templateUrl: './lookup.component.html',
  styleUrls: ['./lookup.component.scss']
})
export class CustomerLookupComponent implements OnInit {
  constructor(public customerService: CustomerService,
    private fb: FormBuilder) { }

  ngOnInit() {
    // TODO: Replace with API data.
    const _dates = [{ date: '1/8/2019', event: 'The Event' }];
    const _extendedLookup = [{ number: 'This number', description: 'B'}];
    const _customFields = [{ name: 'The name', value: 'Value'}];
    const _otherContacts = [{ relationship: 'Spouse', firstName: 'Joel', lastName: 'Embiid'}];
    if (_dates && _dates.length > 0) {
      while (this.datesData && this.datesData.length > 0 && this.datesData.length !== 0) {
        this.datesData.removeAt(0);
      }
      for (const d of _dates) {
        this.datesData.push(this.fb.group({
          event: d.event,
          date: d.date
        }));
      }
    }
    if (_extendedLookup && _extendedLookup.length > 0) {
      while (this.extendedLookupData && this.extendedLookupData.length > 0 && this.extendedLookupData.length !== 0) {
        this.extendedLookupData.removeAt(0);
      }
      for (const e of _extendedLookup) {
        this.extendedLookupData.push(this.fb.group({
          number: e.number,
          description: e.description
        }));
      }
    }
    if (_customFields && _customFields.length > 0) {
      while (this.customFieldsData && this.customFieldsData.length > 0 && this.customFieldsData.length !== 0) {
        this.customFieldsData.removeAt(0);
      }
      for (const c of _customFields) {
        this.customFieldsData.push(this.fb.group({
          name: c.name,
          value: c.value
        }));
      }
    }
    if (_otherContacts && _otherContacts.length > 0) {
      while (this.otherContactsData && this.otherContactsData.length > 0 && this.otherContactsData.length !== 0) {
        this.otherContactsData.removeAt(0);
      }
      for (const o of _otherContacts) {
        this.otherContactsData.push(this.fb.group({
          relationship: o.relationship,
          firstName: o.firstName,
          lastName: o.lastName
        }));
      }
    }
  }

  get customFieldsData(): FormArray {
    return <FormArray>this.customerService.lookupForm.get('customFields');
  }

  get extendedLookupData(): FormArray {
    return <FormArray>this.customerService.lookupForm.get('extendedLookup');
  }

  get datesData(): FormArray {
    return <FormArray>this.customerService.lookupForm.get('dates');
  }

  get otherContactsData(): FormArray {
    return <FormArray>this.customerService.lookupForm.get('otherContacts');
  }

  buildExtendedLookup(): FormGroup {
    return this.fb.group({
      number: '',
      description: ''
    });
  }

  buildDate(): FormGroup {
    return this.fb.group({
      date: '',
      event: ''
    });
  }

  buildCustomFields(): FormGroup {
    return this.fb.group({
      name: '',
      value: ''
    });
  }

  buildOtherContacts(): FormGroup {
    return this.fb.group({
      relationship: '',
      firstName: '',
      lastName: ''
    });
  }

  addExtendedLookup(): void {
    this.extendedLookupData.push(this.buildExtendedLookup());
  }

  addDate(): void {
    this.datesData.push(this.buildDate());
  }

  addCustomFields(): void {
    this.customFieldsData.push(this.buildCustomFields());
  }

  addOtherContacts(): void {
    this.otherContactsData.push(this.buildOtherContacts());
  }
}
