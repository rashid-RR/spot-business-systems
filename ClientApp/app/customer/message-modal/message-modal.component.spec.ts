import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MessageModalComponent } from './message-modal.component';
import { SharedModule } from '../../../app/shared/shared.module';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

describe('MessageModalComponent', () => {
  let component: MessageModalComponent;
  let fixture: ComponentFixture<MessageModalComponent>;
  let mockMatDialogRef;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageModalComponent ],
      imports: [ SharedModule, CKEditorModule ]
    });
    mockMatDialogRef = jasmine.createSpyObj([ 'close' ]);
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(MessageModalComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  it('should create', () => {
    component = new MessageModalComponent(mockMatDialogRef, {});
    expect(component).toBeTruthy();
  });
});
