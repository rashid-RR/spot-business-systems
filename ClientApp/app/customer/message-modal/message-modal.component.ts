declare var require: any;
import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Inject } from '@angular/core';

@Component({
  selector: 'spot-app-message-modal',
  templateUrl: './message-modal.component.html',
  styleUrls: ['./message-modal.component.scss']
})
export class MessageModalComponent implements OnInit {
  template = '';
  templates = [
    {value: '', viewValue: 'None'}
  ];

  email = 'example@email.com';
  emails = [
    {value: 'example@email.com', viewValue: 'example@email.com'}
  ];
  subject = '-';
  // ckeConfig: any;
  public Editor = require('@ckeditor/ckeditor5-build-decoupled-document');

  constructor(public dialogRef: MatDialogRef<MessageModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() { }

  cancel() {
    console.log('this.dialogRef', this.dialogRef);
    this.dialogRef.close();
  }

  public onEditorReady(editor: { ui: { view: { editable: { element: {
    parentElement: { insertBefore: (arg0: any, arg1: any) => void; }; }; };
    toolbar: { element: any; }; }; }; } ) {
    editor.ui.view.editable.element.parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.view.editable.element
    );
  }
}
