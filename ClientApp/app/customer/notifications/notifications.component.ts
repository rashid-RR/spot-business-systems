import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../app/shared';
import { throwError } from 'rxjs';

@Component({
  selector: 'spot-app-customer-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class CustomerNotificationsComponent implements OnInit {
  public selected = 'Emailed';
  public isPanelOpen = false;
  constructor(public customerService: CustomerService) { }

  ngOnInit() {
  }

  get enabledValue(): boolean {
    const enabled = this.customerService.notificationsForm.get('enabled');
    return enabled !== null ? enabled.value : throwError('Error, the enabled form control cannot be found in the notifications component');
  }

  togglePanel() {
    this.isPanelOpen = !this.isPanelOpen;
  }

  enableToggle() {
    const en = this.customerService.notificationsForm.get('enabled');
    if (en) {
      en.setValue(!en.value);
    }
  }
}
