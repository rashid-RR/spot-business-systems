import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../app/shared';
import { SpotCurrency } from '../../../pipes/spot-currency.pipe';
import { FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Constants } from '../../../app/shared/constants';
import { Customer } from '../../../models/customer/customer.interface';
import { RootObjectResponse } from '../../../models/root-object.interface';
import { SpotHttpError } from '../../../models/spot-http-error.model';
import { CustomerMemo } from '../../../models/customer/customer-memo.interface';
import { MatDialog } from '@angular/material';
import { CustomerAddressModalComponent } from '../address-modal/address-modal.component';
import { throwError } from 'rxjs';

@Component({
  selector: 'spot-app-customer-preference',
  templateUrl: './preference.component.html',
  styleUrls: ['./preference.component.scss'],
  providers: [ SpotCurrency ]
})
export class CustomerPreferenceComponent implements OnInit {
  public Starch: string;
  public shirt_pack: string;
  public sweater_pack: string;
  public packaging: string;
  // private customerMemos: CustomerMemo[];

  constructor(public customerService: CustomerService,
    public dialog: MatDialog,
    public spotCurrency: SpotCurrency,
    private route: ActivatedRoute,
    private fb: FormBuilder) {
      this.Starch = 'Medium';
      this.shirt_pack = 'Hang';
      this.sweater_pack = 'Not Speciifed';
      this.packaging = 'No Tissue';
  }

  ngOnInit() {
    const resolvedData: RootObjectResponse<Customer> | SpotHttpError = this.route.snapshot.data[Constants.RESOLVED_CUSTOMER_KEY];
    if (resolvedData instanceof SpotHttpError) {
      console.log(`Preference component error: ${resolvedData.message}`);
    } else {
      if (!resolvedData.Failed) {
        // this.customerMemos = resolvedData.ReturnObject.Memos;
      } else {
        console.log(`Server error. Message: ${resolvedData.Message}`);
      }
    }

    let _memos: CustomerMemo[];
    // _memos = this.customerMemos;
    // TODO: replace with real data.
    _memos = [
      {
        Title: '',
        Text: 'Stubbed data here, but still pulled from the API.',
        Enabled: false,
        LogDisplayRead: false,
        DisplaySubject: false,
        WhenToDisplayMemo: '',
        WhenToDisableMemo: '',
        PrintOnInvoice: false,
        PrintOnStatement: false,
        PrintOnCustomerLabel: false,
        SendToConveyor: false,
        CreatedDateTime: '',
        CreatedByUserInits: '',
        ExpiresDateTime: '',
        ManagedByCustomer: false
      }
    ];

    // Patch memos
    if (_memos && _memos.length > 0) {
      while (this.memoData && this.memoData.length > 0 && this.memoData.length !== 0) {
        this.memoData.removeAt(0);
      }
      for (const memo of _memos) {
        const _text = memo.Text;
        this.memoData.push(this.fb.group({
          memoText: _text
        }));
      }
    }
  }

  get amountValue(): number {
    const amount = this.customerService.preferenceForm.get('amount');
    return amount !== null ? amount.value : throwError('Error, could not find amount control in the preference component');
  }

  get memoData(): FormArray {
    return <FormArray>this.customerService.preferenceForm.get('memos');
  }

  buildMemo(): FormGroup {
    return this.fb.group({
      memoText: ''
    });
  }

  addMemo(): void {
    // TODO: implement a "Memo" modal component here.
    this.dialog.open(CustomerAddressModalComponent, { maxWidth: 500, data: { title: 'Add New Address' } });
  }

  moveback(value: string): void {
    switch (value) {
      case 'Medium':
        this.Starch = 'Small';
        break;
      case 'Small':
        this.Starch = 'Large';
        break;
      case 'Large':
        this.Starch = 'Medium';
        break;
      case 'Hang':
        this.shirt_pack = 'HangPrevious';
        break;
      case 'HangPrevious':
        this.shirt_pack = 'HangNext';
        break;
      case 'HangNext':
        this.shirt_pack = 'Hang';
        break;
      case 'No_Speciifed':
        this.sweater_pack = 'Specified_P';
        break;
      case 'Specified_P':
        this.sweater_pack = 'Specified_N';
        break;
      case 'Specified_N':
        this.sweater_pack = 'No_Speciifed';
        break;
      case 'No_Tissue':
        this.packaging = 'Tissue_P';
        break;
      case 'Tissue_P':
        this.packaging = 'Tissue_N';
        break;
      case 'Tissue_N':
        this.packaging = 'No_Tissue';
        break;
    }
  }

  moveforward(value: string): void {
    switch (value) {
      case 'Medium':
        this.Starch = 'Large';
        break;
      case 'Small':
        this.Starch = 'Medium';
        break;
      case 'Large':
        this.Starch = 'Small';
        break;
      case 'Hang':
        this.shirt_pack = 'HangNext';
        break;
      case 'HangNext':
        this.shirt_pack = 'HangPrevious';
        break;
      case 'HangPrevious':
        this.shirt_pack = 'Hang';
        break;
      case 'No_Speciifed':
        this.sweater_pack = 'Specified_N';
        break;
      case 'Specified_N':
        this.sweater_pack = 'Specified_P';
        break;
      case 'Specified_P':
        this.sweater_pack = 'No_Speciifed';
        break;
      case 'No_Tissue':
        this.packaging = 'Tissue_N';
        break;
      case 'Tissue_N':
        this.packaging = 'Tissue_P';
        break;
      case 'Tissue_P':
        this.packaging = 'No_Tissue';
        break;
    }
  }
}
