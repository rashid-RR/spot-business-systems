import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'ClientApp/app/shared';
import { FormArray, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'spot-app-customer-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class CustomerPricingComponent implements OnInit {
  priceTable = [
    {id: 1, name: 'Option 1'},
    {id: 2, name: 'Option 2'},
    {id: 3, name: 'Option 3'},
    {id: 4, name: 'Option 4'},
    {id: 5, name: 'Option 5'}
  ];
  level = [
    {id: 1, name: 'Option 1'},
    {id: 2, name: 'Option 2'},
    {id: 3, name: 'Option 3'},
    {id: 4, name: 'Option 4'},
    {id: 5, name: 'Option 5'}
  ];
  authorities = [
    {id: 1, name: 'Option 1'},
    {id: 2, name: 'Option 2'},
    {id: 3, name: 'Option 3'},
    {id: 4, name: 'Option 4'},
    {id: 5, name: 'Option 5'}
  ];
  constructor(public customerService: CustomerService,
    private fb: FormBuilder) { }

  ngOnInit() {
    const _taxAuthorities = [{authority: 'It'}];
    if (_taxAuthorities && _taxAuthorities.length > 0) {
      while (this.taxAuthoritiesData && this.taxAuthoritiesData.length > 0 && this.taxAuthoritiesData.length !== 0) {
        this.taxAuthoritiesData.removeAt(0);
      }
      for (const tax of _taxAuthorities) {
        this.taxAuthoritiesData.push(this.fb.group({
          authority: tax.authority
        }));
      }
    }
   }

  get taxAuthoritiesData(): FormArray {
    return <FormArray>this.customerService.pricingForm.get('taxAuthorities');
  }

  buildTaxAuthorities(): FormGroup {
    return this.fb.group({
      authority: ''
    });
  }

  addTaxAuthorities(): void {
    this.taxAuthoritiesData.push(this.buildTaxAuthorities());
  }
}
