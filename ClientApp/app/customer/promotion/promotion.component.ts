import { Component, OnInit } from '@angular/core';


export interface DiscountGroup {
  value: string;
  viewValue: string;
}
export interface RewardsProgram {
  value: string;
  viewValue: string;
}
export interface CharityProgram {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'spot-app-customer-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss']
})
export class CustomerPromotionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  discounts: DiscountGroup[] = [
    {value: 'emp-0', viewValue: 'Employee-50%'},
    {value: 'admin-1', viewValue: 'Admin-80%'},
    {value: 'moderator-2', viewValue: 'moderator-40%'}
  ];
  rewards: RewardsProgram[] = [
    {value: 'none1-0', viewValue: 'None'},
    {value: 'yes-1', viewValue: 'Yess'},
    {value: 'Demo-1', viewValue: 'Demo'}
  ];
  charity: CharityProgram[] = [
    {value: 'none2-0', viewValue: 'None'},
    {value: 'yes-2', viewValue: 'Yess'},
    {value: 'Demo-2', viewValue: 'Demo'}
  ];

}
