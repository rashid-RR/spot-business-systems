import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'ClientApp/app/shared';

@Component({
  selector: 'spot-app-customer-referral',
  templateUrl: './referral.component.html',
  styleUrls: ['./referral.component.scss']
})
export class CustomerReferralComponent implements OnInit {
  referralSources = [
    {id: 1, name: 'Option 1'},
    {id: 2, name: 'Option 2'},
    {id: 3, name: 'Option 3'},
    {id: 4, name: 'Option 4'},
    {id: 5, name: 'Option 5'},
  ];
  constructor(public customerService: CustomerService) { }

  ngOnInit() { }
}
