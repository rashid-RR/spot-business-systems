import { Component, OnInit } from '@angular/core';

export interface LOC {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'spot-app-customer-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class CustomerSettingComponent implements OnInit {
  constructor() { }
  selected = 'Fulton Store';

  Location: LOC[] = [
    {value: 'Demo-0', viewValue: 'Fulton Store'},
    {value: 'Demo-1', viewValue: 'Demo 2'},
    {value: 'Demo-2', viewValue: 'Demo 3'}
  ];

  ngOnInit() {
  }

}
