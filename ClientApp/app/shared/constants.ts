export class Constants {
    public static TOKEN_KEY = 'spot-token';
    public static CUSTOMER_DATA_CACHE_KEY = 'spot-customer-in-cache';
    public static TRUE = 'true';
    public static FALSE = 'false';
    public static API_URL_CHANNEL_PATH = '/t';
    public static GET_TOKEN_REQUEST_TYPE = 'GetToken';
    public static LOGIN_REQUEST_TYPE = 'Login';
    public static CUSTOMER_RETRIEVE_BY_ID_REQUEST_TYPE = 'CustomerRetrieveByID';
    public static RESOLVED_CUSTOMER_KEY = 'resolvedCustomer';
    public static SAVE_CUSTOMER_REQUEST_TYPE = 'CustomerSave';
}

