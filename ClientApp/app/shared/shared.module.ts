import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MatDialogModule, MatInputModule, MatSelectModule, MatFormFieldModule, MatListModule, MatMenuModule, MatToolbarModule, MatIconModule,
  MatProgressSpinnerModule, MatButtonModule, MatChipsModule, MatDividerModule, MatGridListModule, MatCheckboxModule, MatCardModule,
  MatRadioModule, MAT_DIALOG_DEFAULT_OPTIONS, MatTabsModule, MatSidenavModule } from '@angular/material';
  import { SpotCurrency } from '../../pipes/spot-currency.pipe';
  import { PhoneMaskDirective } from './../../directives/phone-mask.directive';
  import { CardExpiryDirective } from './../directives/card-expiry.directive';

@NgModule({
  imports: [
    CommonModule,
    NgxDatatableModule,
    FormsModule, ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    MatRadioModule, MatInputModule, MatSelectModule, MatFormFieldModule, MatListModule, MatMenuModule, MatToolbarModule, MatIconModule,
    MatProgressSpinnerModule, MatButtonModule, MatChipsModule, MatDividerModule, MatGridListModule, MatCheckboxModule, MatCardModule,
    MatDialogModule, MatTabsModule, MatSidenavModule
  ],
  exports: [
    NgbModule,
    FormsModule, ReactiveFormsModule,
    NgxDatatableModule,
    HttpClientModule,
    MatRadioModule, MatInputModule, MatSelectModule, MatFormFieldModule, MatListModule, MatMenuModule, MatToolbarModule, MatIconModule,
    MatProgressSpinnerModule, MatButtonModule, MatChipsModule, MatDividerModule, MatGridListModule, MatCheckboxModule, MatCardModule,
    MatDialogModule, MatTabsModule, SpotCurrency, PhoneMaskDirective, MatSidenavModule, CardExpiryDirective
  ],
  providers: [
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true} }
  ],
  declarations: [SpotCurrency, PhoneMaskDirective, CardExpiryDirective],
  entryComponents: [
  ],
})
export class SharedModule { }
