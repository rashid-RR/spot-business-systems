// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  accountKey: 'devapix',
  gatewayUrl: 'https://devapi.spotpos.com',
  securityId: 'C5C4A58D-8EFD-4AA6-AF7B-8A80FA79C2FA',
  sessionId: '2ef2a39a-704f-4ec6-837b-7e2a855814f5',
  serviceId_mgr: '2135F8A6-E9F5-4854-B91E-06E43391BF6F'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
