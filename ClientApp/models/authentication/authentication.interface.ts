export interface AuthenticationResponse {
    UserID: string;
    Username: string;
    LastName: string;
    FirstName: string;
    Initials: string;
    EmailAddress: string;
    PrincipalSPAccountNodeID: string;
    Birthdate: string;
    LanguageID: 227;
    EmployeeID: string;
    MobilePhoneNumber: string;
    SessionID: string;
    PublishableInstanceID: string;
}
