export interface ICredentials {
    Username: string;
    Password: string;
    DevicePlatform: string;
    DeviceID: string;
    DeviceName: string;
    DeviceOSVersion: number;
    DeviceLanguage: string;
    DeviceTimezone: string;
    AppVersion: number;
    LoginType: string;
    UserValidationStatus: boolean;
}

export class Credentials implements ICredentials {
    Username: string;
    Password: string;
    DevicePlatform: string;
    DeviceID: string;
    DeviceName: string;
    DeviceOSVersion: number;
    DeviceLanguage: string;
    DeviceTimezone: string;
    AppVersion: number;
    LoginType: string;
    UserValidationStatus: boolean;

    constructor(userName: string, password: string, devicePlatform: string, deviceId: string,
            deviceName: string, deviceOsVersion: number, deviceLanguage: string, deviceTimezone: string,
            appVersion: number, loginType: string, userValidationStatus: boolean) {
        this.Username = userName;
        this.Password = password;
        this.DevicePlatform = devicePlatform;
        this.DeviceID = deviceId;
        this.DeviceName = deviceName;
        this.DeviceOSVersion = deviceOsVersion;
        this.DeviceLanguage = deviceLanguage;
        this.DeviceTimezone = deviceTimezone;
        this.AppVersion = appVersion;
        this.LoginType = loginType;
        this.UserValidationStatus = userValidationStatus;
     }
}
