import { Address } from './address.interface';
import { Location } from './location.interface';

export interface AddressAndLocation {
    Address: Address;
    Location: Location;
}
