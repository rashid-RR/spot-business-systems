export interface Address {
    AddressTypeID: number;
    Addressee: string;
    AddressMap: string;
    Address1: string;
    Address2: string;
    City: string;
    State: string;
    Zip: string;
    PostalCodeMask: string;
    AddressMapOrLine1: string;
}
