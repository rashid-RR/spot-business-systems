export interface CustomerMemo {
    Title: string;
    Text: string;
    Enabled: boolean;
    LogDisplayRead: boolean;
    DisplaySubject: boolean;
    WhenToDisplayMemo: string;
    WhenToDisableMemo: string;
    PrintOnInvoice: boolean;
    PrintOnStatement: boolean;
    PrintOnCustomerLabel: boolean;
    SendToConveyor: boolean;
    CreatedDateTime: Date | string;
    CreatedByUserInits: string;
    ExpiresDateTime?: Date | string;
    ManagedByCustomer: boolean;
}
