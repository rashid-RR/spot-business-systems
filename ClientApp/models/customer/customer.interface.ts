import { AddressAndLocation } from './address-and-location.interface';
import { Phone } from './phone.interface';
import { Preference } from './preference.interface';
import { DiscountGroup } from './discount-group.interface';
import { CustomerMemo } from './customer-memo.interface';
import { CustomerDateEntry } from './dates.interface';
import { TaxAuthority } from './tax-authority.interface';

export interface Customer {
    CustomerID: string;
    LexicalKey: string;
    StoreID: string;
    Active: string;
    CustomerType: string;               // will map to entry in CompanySettings.CustomerTypes
    Title: string;                      // will map to entry in CompanySettings.CustomerTitles
    LastName: string;
    FirstName: string;
    Contact: string;
    Position: string;                   // will map to entry in CompanySettings.CustomerPositions
    AddressesAndLocations: AddressAndLocation[];
    Phones: Phone[];
    EmailAddresses: string[];
    IgnoreDuplicateEmail: boolean;
    Preferences: Preference[];
    PriceTableName: string;             // will map to entry in CompanySettings.PriceTables
    PriceLevel: string;                 // will map to entry in CompanySettings.PriceLevels
    ARAccountType: string;              // will map to entry in CompanySettings.CustomerARAccountTypes
    ARMasterAccountID: string;            // references the CustomerID of the 'owner' of the AR account
    ARAccountNumber: string;
    ARActive: boolean;
    ARCreditLimit: number;
    ARBillingGroupName: string;         // will map to entry in CompanySettings.CustomerARBillingGroups
    VIP: boolean;
    MyReferralCode: string;             // the referral code used for referrals by this customer
    ReferralSource: string;             // will map to entry in CompanySettings.CustomerReferralSources
    ReferralDetail: string;
    ReferringRecordID: string;
    ReferringCustomerID: string;
    ReferringCustomerName: string;
    CustomerGroupName: string;          // will map to entry in CompanySettings.CustomerGroups
    DiscountGroup: DiscountGroup;       // will map to entry in CompanySettings.CustomerDiscountGroups
    SignupDateTime: Date | string;
    IsLockerCustomer: boolean;          // deprecated
    LockerCustomer: string;             // "N", "Y" or "P" (prompt)
    LockerPIN: string;
    LockerOnly: boolean;
    LockerNumberRequest: string;
    EnableHSL: boolean;
    EnableReusableGarmentBags: boolean;
    LastVisitDate: Date | string;
    TenderInfo: string;
    Memos: CustomerMemo[];
    Dates: CustomerDateEntry[];
    RouteID: string;
    RouteSignupDateTime: Date | string;
    StopID: string;
    StopNumber: number;
    SubStopOrdinal: number;
    StopDaysToVisit: string;
    StopDaysToVisitEnum: number;
    StopInstructions: string;
    PickupType: number;
    PickupAddressTypeID: number;
    PickupDaysToVisit: string;
    PickupDaysToVisitEnum: number;
    PickupLocation: string;
    PickupInstructions: string;
    PickupAdditionalInfo: string;
    DeliveryType: number;
    DeliveryAddressTypeID: number;
    DeliveryDaysToVisit: string;
    DeliveryDaysToVisitEnum: number;
    DeliveryLocation: string;
    DeliveryInstructions: string;
    DeliveryAdditionalInfo: string;
    PreferredTimeRange: string;
    BagTagCount: number;
    ScanAtDelivery: string;
    VisitReminder: boolean;
    TaxAuthorities: TaxAuthority[];
    IgnoreValidationErrors: boolean;
}
