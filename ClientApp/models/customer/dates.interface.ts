export interface CustomerDateEntry {
    DateType: string;               // will map to entry in CompanySettings.CustomerDates
    DateValue: Date | string;
    Immutable: boolean;
}
