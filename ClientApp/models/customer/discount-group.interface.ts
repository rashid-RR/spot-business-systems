export interface DiscountGroup {
    ID: string;
    Name: string;
    DiscountPercentage: number;
    DiscountPeriod: string;
    DiscountStarting: string;
    Expires: Date | string;
    TimeBasedExpires: number;
    MinimumPieceCount: number;
    MaximumVisitAmount: number;
    DiscountsByDepartment: any;
}
