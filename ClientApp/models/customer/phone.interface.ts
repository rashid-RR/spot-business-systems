export interface Phone {
    PhoneType: string;
    Number: string;
    Extension: string;
    PhoneMask: string;
}
