export interface Preference {
    Name: string;
    Description: string;
    Value: string;
}
