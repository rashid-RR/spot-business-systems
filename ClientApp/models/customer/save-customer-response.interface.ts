export interface SaveCustomerResponse {
    Success: boolean;
    CustomerID: string;
    LexicalKey: string;
}