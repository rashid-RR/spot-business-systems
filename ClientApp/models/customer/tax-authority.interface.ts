export interface TaxAuthority
{
    Name: string;
    TaxID: string;
    Exempt1: boolean;
    Exempt2: boolean;
}