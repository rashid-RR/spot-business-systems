export interface RootObjectResponse<T> {
    HostName: string;
    Version: string;
    Failed: boolean;
    Message: string;
    MessageDetails: string;
    ValidationInfo?: any;
    ReturnObject: T;
}
