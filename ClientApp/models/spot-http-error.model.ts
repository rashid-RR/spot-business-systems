export class SpotHttpError {
    errorStatus: number;
    errorNumber: number;
    message: string;
    friendlyMessage: string;

    constructor(errorStatus: number, errorNumber: number, message: string, friendlyMessage: string) {
        this.errorStatus = errorStatus;
        this.errorNumber = errorNumber;
        this.message = message;
        this.friendlyMessage = friendlyMessage;
    }
}
