import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { RootObjectResponse } from '../../models/root-object.interface';
import { Session } from '../../models/session.interface';
import { throwError, Observable } from 'rxjs';
import { Credentials } from '../../models/authentication/credentials-model';
import { AuthenticationResponse } from '../../models/authentication/authentication.interface';
import { Customer } from '../../models/customer/customer.interface';
import { SpotHttpError } from '../../models/spot-http-error.model';
import { Constants } from '../../app/shared/constants';
import { SaveCustomerResponse } from '../../models/customer/save-customer-response.interface';

@Injectable({
    providedIn: 'root'
})
export class CustomerDataService {

    constructor(private http: HttpService) { }
    private tokenKey = Constants.TOKEN_KEY;

    setTokenInStorage(): Promise<boolean> {

        return new Promise((resolve, reject) => {
            this.http.getToken().subscribe(
                (data: RootObjectResponse<Session>) => {
                    if (!data.Failed) {
                        const tk = data.ReturnObject.SessionID;
                        if (tk) {
                            console.log(`Token: ${tk}`);
                            localStorage.setItem(this.tokenKey, tk);
                        }
                    } else {
                        if (data.Message) {
                            throwError(data.Message);
                        }
                    }
                    console.log("setTokenInStorage executed");
                    resolve(true);
                }
            );
        });
    }

    setLoginTokenInStorage(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let credentials: Credentials;
            credentials = new Credentials('clerk', 'clerk', '', '', '', 0, '', '', 0, '', false);
            // TODO: Remove hard-coded username and password.
            credentials.Username = 'clerk';
            credentials.Password = 'clerk';
            const token = localStorage.getItem(this.tokenKey);
            if (!token) {
                throwError('Error, the GetToken call was not called first or was not successful.');
                return;
            }
            this.http.login(credentials, token).subscribe(
                (data: RootObjectResponse<AuthenticationResponse>) => {
                    if (!data.Failed) {
                        const tk = data.ReturnObject.SessionID;
                        console.log(`Login successful, Token: ${tk}`);
                        localStorage.setItem(this.tokenKey, tk);
                    } else {
                        if (data.Failed && data.Message) {
                            console.log(data.Message);
                        }
                    }
                    console.log("setLoginTokenInStorage executed 2nd")
                    resolve(true);
                }
            );
        });
    }

    getCustomer(): Observable<RootObjectResponse<Customer> | SpotHttpError> {
        const token = localStorage.getItem(this.tokenKey) || '';
        if (!token || token === '') {
            const mesg = 'could not get the token from local storage';
            throwError(mesg);
        }
        // TODO: Don't hard-code Customer GUID.
        return this.http.getCustomer('CF7E6566-95A9-4D75-A177-D4DE6966A2D9', token);
    }

    saveCustomer(customer: Customer): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const token = localStorage.getItem(this.tokenKey) || '';
            if (!token || token === '') {
                const mesg = 'could not get the token from local storage';
                throwError(mesg);
            }
            this.http.saveCustomer(customer, token)
                .subscribe(
                    (data: RootObjectResponse<SaveCustomerResponse>) => {
                        if (!data.Failed) {
                            throwError(`Save Customer Failed with message: ${data.Message}`);
                        } else {
                            if (data.ReturnObject && data.ReturnObject.Success === true) {
                                throwError(`Finished saving the customer ID: ${data.ReturnObject.CustomerID}`);
                            }
                        }
                        resolve(true);
                    }
                );
        });
    }
}
