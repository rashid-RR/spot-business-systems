import { Injectable, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomerService implements OnInit {
  informationForm: FormGroup;
  preferenceForm: FormGroup;
  creditCardForm: FormGroup;
  notificationsForm: FormGroup;
  referralForm: FormGroup;
  lookupForm: FormGroup;
  pricingForm: FormGroup;
  promotionsForm: FormGroup;
  settingsForm: FormGroup;

  constructor(private fb: FormBuilder) {
      // Information
      this.informationForm = this.fb.group({
        firstName: ['Jordan', Validators.compose([Validators.required])],
        lastName: ['Gurney', Validators.compose([Validators.required])],
        customerType: ['Individual', Validators.compose([Validators.required])],
        emails: this.fb.array([ ]),
        vip: ['false'],
        phones: this.fb.array([ ]),
        phoneNumber: ['(123) 456-7890', Validators.compose([Validators.required])],
        password: ['00000000', Validators.compose([Validators.required])],
        addresses: this.fb.array([ ])
      });

      // Preference
      this.preferenceForm = this.fb.group({
        starch: ['Starch'],
        shirtPack: ['Shirt Pack', Validators.compose([Validators.required])],
        amount: [50.25],
        memos: this.fb.array([ ])
      });

      // Credit Card
      this.creditCardForm = this.fb.group({
        enabled: [true],
        usage: ['Never', Validators.compose([Validators.required])],
        counterCreditCardCharge: ['StoreDefault', Validators.compose([Validators.required])],
        routeCreditCardCharge: ['StoreDefault', Validators.compose([Validators.required])],
        creditCardType: ['Primary', Validators.compose([Validators.required])],
        nameOnCard: ['Jordan Gurney', Validators.compose([Validators.required])],
        lastFourDigits: [1040, Validators.compose([Validators.required])],
        expires: ['01 / 24', Validators.compose([Validators.required])]
      });

      // Notification
      this.notificationsForm = this.fb.group({
        enabled: [true, Validators.compose([Validators.required])],
        receipt: ['Emailed', Validators.compose([Validators.required])]
      });

      // Referral
      this.referralForm = this.fb.group({
        referralSource: ['Already Existing Customer', Validators.compose([])],
        referralDetails: [''],
        customerReferral: [''],
        customerReferralDetails: ['']
      });

      // Lookup
      this.lookupForm = this.fb.group({
        dates: this.fb.array([ ]), // 2 elements inside: event, date
        extendedLookup: this.fb.array([ ]), // 2 elements inside: number, description
        customFields: this.fb.array([ ]), // 2 elements inside: name, value
        otherContacts: this.fb.array([ ]) // 3 elements inside: relationship, firstName, lastName
      });

      // Pricing
      this.pricingForm = this.fb.group({
        priceTable: '',
        level: '',
        authority: '',
        taxAuthorities: this.fb.array([ ]) // 1 element: authority
      });

      // Promotions
      this.promotionsForm = this.fb.group({
        discountGroup: '',
        expiresOn: '',
        rewardsProgram: '',
        charityProgram: ''
      });

      // Settings
      this.settingsForm = this.fb.group({
        cashEnabled: [true, Validators.compose([Validators.required])],
        checkEnabled: [true, Validators.compose([Validators.required])],
        creditCardEnabled: [true, Validators.compose([Validators.required])]
      });
  }
  ngOnInit() { }
}
