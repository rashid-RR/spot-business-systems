import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

@Injectable()
export class HttpCacheService {

  private requests: any = { };

  constructor() { }

  put(requestType: string, response: HttpResponse<any>): void {
    this.requests[requestType] = response;
  }

  get(requestType: string): HttpResponse<any> | undefined {
    return this.requests[requestType];
  }

  invalidateRequest(requestType: string): void {
    this.requests[requestType] = undefined;
  }

  invalidateCache(): void {
    this.requests = { };
  }
}
