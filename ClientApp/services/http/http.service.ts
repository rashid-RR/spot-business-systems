/*
 * This service will serve to facilitate communication between app views and the web services
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Credentials } from '../../models/authentication/credentials-model';
import * as Base64 from 'crypto-js/enc-base64';
import * as UTF8 from 'crypto-js/enc-utf8';
import { Customer } from '../../models/customer/customer.interface';
import { RootObjectResponse } from '../../models/root-object.interface';
import { Session } from '../../models/session.interface';
import { AuthenticationResponse } from '../../models/authentication/authentication.interface';
import { Constants } from '../../app/shared/constants';

@Injectable()
export class HttpService {
  domainBaseUrl = environment.gatewayUrl;
  apiBaseUrl = Constants.API_URL_CHANNEL_PATH;
  baseUrl = this.domainBaseUrl + this.apiBaseUrl;

  constructor(private http: HttpClient) { }

  getToken(): Observable<RootObjectResponse<Session>> {
    const requestType = Constants.GET_TOKEN_REQUEST_TYPE;
    return this.http.post<RootObjectResponse<Session>>(this.baseUrl, { RequestType: requestType });
  }

  login(creds: Credentials, sessionId: string): Observable<RootObjectResponse<AuthenticationResponse>> {
    const requestType = Constants.LOGIN_REQUEST_TYPE;
    const Body = Base64.stringify(UTF8.parse(JSON.stringify(creds)));
    return this.http.post<RootObjectResponse<AuthenticationResponse>>(this.baseUrl, { Body: Body, RequestType: requestType, SessionID: sessionId });
  }

  getCustomer(CustomerID: string, sessionId: string): Observable<RootObjectResponse<Customer>> {
    const requestType = Constants.CUSTOMER_RETRIEVE_BY_ID_REQUEST_TYPE;
    const Body = Base64.stringify(UTF8.parse(JSON.stringify({ CustomerID: CustomerID })));
    return this.http.post<RootObjectResponse<Customer>>(this.baseUrl, { Body: Body, RequestType: requestType, SessionID: sessionId });
  }

  saveCustomer(customer: Customer, sessionId: string): Observable<any> {
    const requestType = Constants.SAVE_CUSTOMER_REQUEST_TYPE;
    const Body = Base64.stringify(UTF8.parse(JSON.stringify({ Body: customer })));
    return this.http.post<any>(this.baseUrl, { Body: Body, RequestType: requestType, SessionID: sessionId });
  }
}
