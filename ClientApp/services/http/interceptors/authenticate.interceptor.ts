import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from '../../../environments/environment';


@Injectable()
export class AuthenticateInterceptor implements HttpInterceptor {
    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const body = req.body;
        if (body) {
            body.ServiceID = environment.serviceId_mgr;
            body.SecurityID = environment.securityId;
            body.AccountKey = environment.accountKey;
        }
        const modifiedReq = req.clone({ setHeaders: {
                'Content-Type': 'application/json',
                'Accept': 'application/json' }});
        return next.handle(modifiedReq);
    }
};