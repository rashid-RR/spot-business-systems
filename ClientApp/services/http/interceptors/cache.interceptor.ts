import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpCacheService } from '../http-cache.service';
import { Constants } from '../../../app/shared/constants';


@Injectable()
export class CacheInterceptor implements HttpInterceptor {

    constructor(private cacheService: HttpCacheService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // pass along non-cacheable requests
        const customerRetrieveByIdRequestType = Constants.CUSTOMER_RETRIEVE_BY_ID_REQUEST_TYPE;
        const saveCustomerRequestType = Constants.SAVE_CUSTOMER_REQUEST_TYPE;

        // attempt to retrieve a cached response
        const isCustomerRetrieveByIdRequest = req && req.body && req.body.RequestType && req.body.RequestType === customerRetrieveByIdRequestType;
        if (isCustomerRetrieveByIdRequest) {
            const cachedResponse: HttpResponse<any> | undefined = this.cacheService.get(customerRetrieveByIdRequestType);
            // return cached response
            if (cachedResponse) {
                console.log(`Returning a cached response: ${cachedResponse.url}`);
                return of(cachedResponse);
            }
        }

        // If we're saving the customer, invalidate the cached retrieve response.
        if (req && req.body && req.body.RequestType && req.body.RequestType === saveCustomerRequestType) {
            this.cacheService.invalidateRequest(customerRetrieveByIdRequestType);
        }

        // send request to server and add response to cache
        return next.handle(req)
            .pipe(
                tap(ev => {
                    if (ev instanceof HttpResponse && isCustomerRetrieveByIdRequest) {
                        console.log(`Adding item to cache:, ${req.body}`);
                        this.cacheService.put(customerRetrieveByIdRequestType, ev);
                        // Store the fact that we are now operating in the cache to avoid logging in again.
                        const customerDataCacheKey = Constants.CUSTOMER_DATA_CACHE_KEY;
                        const trueKey = Constants.TRUE;
                        if (customerDataCacheKey && trueKey) {
                            localStorage.setItem(customerDataCacheKey, trueKey);
                        }
                    }
                })
            );
    }
}
