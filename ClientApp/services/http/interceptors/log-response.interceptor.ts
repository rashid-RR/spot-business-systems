import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { Constants } from '../../../app/shared/constants';

@Injectable()
export class LogResponseInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req)
        /** We will use the pipe in this case to do additional processing of
             * the response before its returned to the method that called the  http client*/
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    /*let data = {};
                    data = {
                        reason: error && error.error.reason ? error.error.reason : '',
                        status: error.status
                    };*/
                    if (error.status === 401) {
                        // this.router.navigate(['/login']);
                        console.log('We would normally route back to the login page...');
                        // Now, remove the Customer is in cache boolean variable from local storage to force us to log in again.
                        const customerDataCacheKey = Constants.CUSTOMER_DATA_CACHE_KEY;
                        if (customerDataCacheKey) {
                            localStorage.removeItem(customerDataCacheKey);
                        }
                    }
                    return throwError(error);
                })
            );
  }
}
