import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { RootObjectResponse } from '../../models/root-object.interface';
import { Customer } from '../../models/customer/customer.interface';
import { SpotHttpError } from '../../models/spot-http-error.model';
import { CustomerDataService } from '../customer/customer-data.service';
import { Observable, of, from } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { Constants } from '../../app/shared/constants';

@Injectable()
export class CustomerResolverService implements Resolve<RootObjectResponse<Customer> | SpotHttpError> {

    constructor(private customerDataService: CustomerDataService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<RootObjectResponse<Customer> | SpotHttpError> {
        const customerDataCacheKey = Constants.CUSTOMER_DATA_CACHE_KEY;
        const trueKey = Constants.TRUE;
        let customerIsInCache = false;
        if (customerDataCacheKey && trueKey) {
            const cachedValue = localStorage.getItem(customerDataCacheKey);
            if (cachedValue === trueKey) {
                customerIsInCache = true;
            }
        }
        if (!customerIsInCache) {
            return this.customerInfo();
        }
        return this.customerDataService.getCustomer()
            .pipe(
                catchError(err => of(err))
            );
    }

    customerInfo(): Observable<RootObjectResponse<Customer> | SpotHttpError> {
        return from<any>(this.customerDataService.setTokenInStorage())
            .pipe(
                // make this request to call the second API and discard the first result
                switchMap(d => from<any>(this.customerDataService.setLoginTokenInStorage())
                 .pipe(
                     // make this request to call the third API and discard the first and second
                     // results, so the data from getCustomer is the only one returned to the resolver
                    switchMap(results => this.customerDataService.getCustomer())
                )), catchError(err => of(err))
             );
    }
}
