import { Injectable, ErrorHandler } from '@angular/core';
import { SpotHttpError } from '../models/spot-http-error.model';
import { throwError } from 'rxjs';

@Injectable()
export class SpotErrorHandlerService implements ErrorHandler {
    handleError(error: any): void {
        let spotHttpError = new SpotHttpError(error.status, 200, error.statusText, 'An error occurred. Please try again.');
        console.log(spotHttpError); // TODO: Make this more advanced.
        throwError(error);
    }
    constructor() { }
}

